package com.ubyfol.ubytrack;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender.SendIntentException;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class LocationProvider implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {
    private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    public static final String TAG = LocationProvider.class.getSimpleName();
    private Activity mActivity;
    private Context mContext;
    private GoogleApiClient mGoogleApiClient;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest = LocationRequest.create().setPriority(100).setInterval(600000).setFastestInterval(1000);

    public interface LocationCallback {
        void handleNewLocation(Location location);
    }

    public LocationProvider(Context context, LocationCallback callback) {
        this.mActivity = (Activity) context;
        this.mGoogleApiClient = new Builder(context).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        this.mLocationCallback = callback;
        this.mContext = context;
    }

    public void connect() {
        this.mGoogleApiClient.connect();
    }

    public void disconnect() {
        if (this.mGoogleApiClient.isConnected()) {
            this.mGoogleApiClient.disconnect();
        }
    }

    public void onConnected(Bundle bundle) {
        Log.i(TAG, "Location services connected.");
        if (ActivityCompat.checkSelfPermission(this.mActivity, "android.permission.ACCESS_FINE_LOCATION") == 0 || ActivityCompat.checkSelfPermission(this.mActivity, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            Location location = LocationServices.FusedLocationApi.getLastLocation(this.mGoogleApiClient);
            if (location == null) {
                LocationServices.FusedLocationApi.requestLocationUpdates(this.mGoogleApiClient, this.mLocationRequest, this);
                return;
            } else {
                this.mLocationCallback.handleNewLocation(location);
                return;
            }
        }
        ActivityCompat.requestPermissions(this.mActivity, new String[]{"android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION"}, 1);
    }

    public void onConnectionSuspended(int i) {
    }

    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution() && (this.mContext instanceof Activity)) {
            try {
                this.mActivity = (Activity) this.mContext;
                connectionResult.startResolutionForResult(this.mActivity, 9000);
                return;
            } catch (SendIntentException e) {
                e.printStackTrace();
                return;
            }
        }
        Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
    }

    public void onLocationChanged(Location location) {
        this.mLocationCallback.handleNewLocation(location);
    }
}
