package com.ubyfol.ubytrack;

import android.location.Location;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.ubyfol.ubytrack.LocationProvider.LocationCallback;

public class Main extends AppCompatActivity implements LocationCallback {
    private String ideDis = "";
    private LocationProvider mLocationProvider;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);
        PermissionUtils.validate(this, 0, "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION", "android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE", "android.permission.RECEIVE_BOOT_COMPLETED");
        this.ideDis = Secure.getString(getApplicationContext().getContentResolver(), "android_id");
        ((Button) findViewById(R.id.btnLoc)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ((TextView) findViewById(R.id.txtIde)).setText("ID: " + ideDis);
                Main.this.mLocationProvider = new LocationProvider(Main.this, Main.this);
                Main.this.mLocationProvider.connect();
            }
        });
    }

    public void handleNewLocation(Location l) {
        ((TextView) findViewById(R.id.txtLat)).setText("Latitude:" + l.getLatitude());
        ((TextView) findViewById(R.id.txtLon)).setText("Longitude:" + l.getLongitude());
        ((TextView) findViewById(R.id.txtErr)).setText(new SendLoc(this).execute(new Object[]{Double.valueOf(l.getLatitude()), Double.valueOf(l.getLongitude())}).toString());
    }
}
