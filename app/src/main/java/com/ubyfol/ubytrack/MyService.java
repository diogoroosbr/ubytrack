package com.ubyfol.ubytrack;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.IBinder;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy.Builder;
import android.provider.Settings.Secure;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MyService extends Service {
    private static final float LOCATION_DISTANCE = 200.0f; //200 metros
    private static final int LOCATION_INTERVAL = 1200000; //20 min
    private static final String TAG = "BOOMBOOMTESTGPS";
    String android_id = "";
    String lat = "";
    String lon = "";
    LocationListener[] mLocationListeners;
    private LocationManager mLocationManager = null;

    private class LocationListener implements android.location.LocationListener {
        Location mLastLocation;

        public LocationListener(String provider) {
            this.mLastLocation = new Location(provider);
        }

        public void onLocationChanged(Location l) {
            MyService.this.lat = Double.toString(l.getLatitude());
            MyService.this.lon = Double.toString(l.getLongitude());
            if (new Utils() != null && Utils.isConnected(MyService.this.getApplicationContext())) {
                new SendLoc(MyService.this).execute(new Object[]{Double.valueOf(l.getLatitude()), Double.valueOf(l.getLongitude())});
            }
            this.mLastLocation.set(l);
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            MyService.this.lat = Double.toString(this.mLastLocation.getLatitude());
            MyService.this.lon = Double.toString(this.mLastLocation.getLongitude());
            if (new Utils() != null && Utils.isConnected(MyService.this.getApplicationContext())) {
                try {
                    HttpURLConnection urlConnection = (HttpURLConnection) new URL("http://senior.ubyfol.com.br:8888/avancere/ubyloc/F073GPS.jsf?id=" + MyService.this.android_id + "&la=" + MyService.this.lat + "&lo=" + MyService.this.lon + "&MyService").openConnection();
                    urlConnection.getResponseCode();
                    urlConnection.disconnect();
                } catch (MalformedURLException e2) {
                    e2.printStackTrace();
                } catch (Throwable th3) {
                    th3.printStackTrace();
                }
            }
        }
    }

    public MyService() {
        this.mLocationListeners = new LocationListener[]{new LocationListener("gps"), new LocationListener("network")};
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return flags;
    }

    public void onCreate() {
        if (VERSION.SDK_INT <= 19) {
            StrictMode.setThreadPolicy(new Builder().permitAll().build());
        }
        this.android_id = Secure.getString(getApplicationContext().getContentResolver(), "android_id");
        initializeLocationManager();
        try {
            this.mLocationManager.requestLocationUpdates("gps", LOCATION_INTERVAL, LOCATION_DISTANCE, this.mLocationListeners[0]);
        } catch (SecurityException e3) {
            e3.printStackTrace();
        } catch (IllegalArgumentException e4) {
            e4.printStackTrace();
        }
        try {
            this.mLocationManager.requestLocationUpdates("network", LOCATION_INTERVAL, LOCATION_DISTANCE, this.mLocationListeners[1]);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e2) {
            e2.printStackTrace();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        System.out.println("UBYSRV ONDESTROY");
        Log.i(TAG, "UBYSRV1 ONDESTROY");
        if (this.mLocationManager != null) {
            for (android.location.LocationListener removeUpdates : this.mLocationListeners) {
                try {
                    if (ContextCompat.checkSelfPermission(this, "android.permission.READ_PHONE_STATE") != 0) {
                        this.mLocationManager.removeUpdates(removeUpdates);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void initializeLocationManager() {
        if (this.mLocationManager == null) {
            this.mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }
}
