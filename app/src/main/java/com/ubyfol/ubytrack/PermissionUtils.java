package com.ubyfol.ubytrack;

import android.app.Activity;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import java.util.ArrayList;
import java.util.List;

public class PermissionUtils {
    public static boolean validate(Activity activity, int requestCode, String... permissions) {
        List<String> list = new ArrayList();
        for (String permission : permissions) {
            boolean ok;
            if (ContextCompat.checkSelfPermission(activity, permission) == 0) {
                ok = true;
            } else {
                ok = false;
            }
            if (!ok) {
                list.add(permission);
            }
        }
        if (list.isEmpty()) {
            return true;
        }
        String[] newPermissions = new String[list.size()];
        list.toArray(newPermissions);
        ActivityCompat.requestPermissions(activity, newPermissions, 1);
        return false;
    }
}
