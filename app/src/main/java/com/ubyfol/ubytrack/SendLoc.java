package com.ubyfol.ubytrack;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class SendLoc extends AsyncTask {
    private final Context context;

    public SendLoc(Context c) {
        this.context = c;
    }

    protected void onPreExecute() {
    }

    protected Object doInBackground(Object[] params) {
        String ret = "";
        String ideDis = Secure.getString(this.context.getApplicationContext().getContentResolver(), "android_id");
        if (new Utils() == null || !Utils.isConnected(this.context.getApplicationContext())) {
            return ret;
        }
        try {
            URL url2 = new URL("http://senior.ubyfol.com.br:8888/avancere/ubyloc/F073GPS.jsf?id=" + ideDis + "&la=" + params[0] + "&lo=" + params[1] + "&SendLoc");
            try {
                HttpURLConnection urlConnection = (HttpURLConnection) url2.openConnection();
                urlConnection.getResponseCode();
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String line = "";
                StringBuilder responseOutput = new StringBuilder();
                while (true) {
                    line = br.readLine();
                    if (line != null) {
                        responseOutput.append(line);
                    } else {
                        br.close();
                        urlConnection.disconnect();
                        return ret;
                    }
                }
            } catch (IOException e3) {
                e3.printStackTrace();
                return null;
            } catch (Throwable th3) {
                th3.printStackTrace();
                return null;
            }
        } catch (MalformedURLException e4) {
            e4.printStackTrace();
            return null;
        }
    }

    protected void onPostExecute() {
    }
}
