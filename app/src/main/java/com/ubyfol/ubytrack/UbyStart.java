package com.ubyfol.ubytrack;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import com.google.android.gms.drive.DriveFile;

public class UbyStart extends BroadcastReceiver {
    private static final String TAG = "UbyStart";
    Bundle bundle;
    private Context ctx;
    Intent i;
    private String ideDis = "";
    String phoneState = "";

    public void onReceive(Context context, Intent intent) {
        this.ideDis = Secure.getString(context.getContentResolver(), "android_id");
        if ("android.intent.action.BOOT_COMPLETED".equalsIgnoreCase(intent.getAction())) {
            this.ctx = context;
            this.i = new Intent(context, MyService.class);
            this.i.setAction("STARTFOREGROUND_ACTION");
            this.i.addFlags(DriveFile.MODE_READ_ONLY);
            this.bundle = intent.getExtras();
            this.phoneState = this.bundle.getString("state");
            if (this.phoneState != null) {
                context.startService(this.i);
            } else {
                context.startService(this.i);
            }
        }
    }
}
