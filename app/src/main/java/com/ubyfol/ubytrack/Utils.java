package com.ubyfol.ubytrack;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.os.Build.VERSION;

public class Utils {
    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (VERSION.SDK_INT >= 21) {
            for (Network n : cm.getAllNetworks()) {
                if (cm.getNetworkInfo(n).isConnected()) {
                    return true;
                }
            }
        } else {
            NetworkInfo info = cm.getActiveNetworkInfo();
            if (info != null && info.getState() == State.CONNECTED) {
                return true;
            }
        }
        return false;
    }
}
